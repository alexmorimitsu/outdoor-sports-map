function initMap() {
  // Create a map object and specify the DOM element for display.
  var map = new google.maps.Map(document.getElementById('map-canvas'), {
    center: {lat: -23.558745, lng: -46.731859},
    scrollwheel: true,
    zoom: 12
  });
  
  
  var c = {lat: gon.lat, lng: gon.lng}
  
  var circle = new google.maps.Circle({
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#0000FF',
      fillOpacity: 0.35,
      map: map,
      center: c,
      radius: 100000
    });
  
  gon.lat
}